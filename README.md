# Ircd

This is the working group responsible for operating the
`irc.indymedia.org` IRC network. You can find news and some user
documentation here.

The network has a support channel. To get help from operators, you can
join the `#ircd` channel

For help with IRC in general, see [IRChelp.org](http://www.irchelp.org/).

To access the network with a web browser, you can use <https://chat.indymedia.org/> or <https://chat.koumbit.net/>.

You can also access the network through Tor onion (hidden) services
with the hostname
`akeyxc6hie26nlfylwiuyuf3a4tdwt4os7wiz3fsafijpvbgrkrzx2qd.onion` on
port 6667 without TLS.

Our user documentation is currently limited, and we are in the process of transfering the [old documentation](http://web.archive.org/web/20151113200910/https://docs.indymedia.org/Sysadmin/IrcHowTo). Contributions welcome!

See also the [upstream documentation of the IRC server](http://charybdis.readthedocs.io/en/latest/) and [services](https://github.com/atheme/atheme/wiki).

See also the administrator documentation in:

- [servers](servers) – how to manage the different servers, IRCd documentation, DNS, etc
- [services](services) – how to manage services (NickServ, ChanServ, etc)
- [todo](todo) – ever growing list of things to do on the network
- [news](news) – old list of news item, deprecated, we use global
  notices, mostly

We also use an encrypted mailing list to coordinate,
`ircd@lists.immerda.ch`, see [schleuder
docs](https://schleuder.org/docs/) for usage.

See also the [operator guide](http://charybdis.readthedocs.io/en/latest/) ([shipped with charybdis](https://github.com/charybdis-ircd/charybdis/tree/release/3.5/doc/oper-guide)) for more information on channel flags and IRCd configuration.
