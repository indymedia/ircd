[[_TOC_]]

Configuration
=============

Charybdis
--------

All servers run [Charybdis](http://www.charybdis.io/) for the main IRC server functionality.

Configuration is in `/etc/charybdis/sharedconf`. See [sharedconf](sharedconf) for details on how to propagate that configuration around the different servers.

Charybdis is installed from Debian packages, which is maintained by anarcat. The git repository is in [collab-maint](https://anonscm.debian.org/cgit/collab-maint/charybdis.git/) and you can also see the [package tracker page](http://tracker.debian.org/charybdis) that is quite useful.

See also the [operator guide](https://www.stack.nl/~jilles/irc/charybdis-oper-guide/index.html) ([shipped with charybdis](https://github.com/charybdis-ircd/charybdis/tree/release/3.5/doc/sgml/oper-guide)) for more information on channel flags and IRCd configuration. Some extensions are *not* documented in the manual, however, see the [extensions source code](https://github.com/charybdis-ircd/charybdis/blob/release/3.5/doc/reference.conf#L48) for a list of such directives.

Web chat
--------------

Koumbit setup qwebirc on chat0.koumbit.net. It is reachable through https://chat.koumbit.net/ . This is documented in https://wiki.koumbit.net/QwebIrc

There is also a qwebirc install at https://irc.indymedia.nl/

WTF is http://chat.indymedia.org/? -- redirects to https, which gives a 403 that asks you to contact "admin [at] puscii [punt] nl"

DNS
---

We use round-robin DNS to distribute the load among the different
servers below. When a user connects to irc.indymedia.org, the hostname
will resolve to one of several A records.

See also
--------

* https://we.riseup.net/ircd/services

Server list
===========

Note that this section is just aiming to be a global overview of the machines. Specific state of each server (e.g. version, disk space and other stats) is best described by the machine itself. Each IRC operator *should* have shell access with `sudo` on each of those machines, unless otherwise noted. 

The IRC network is composed of those machines:

che.indymedia.org
-----------------

* New York
* "Linux Virtual Server"
* Tachanka
* Charybdis

chat0.koumbit.net
-----------------

* Montreal
* Koumbit (secondary point of presence)
* Xen DomU
* Charybdis
* Webchat (https://chat.koumbit.net/)
* Note: Koumbit's wildcard SSL private cert is on this machine, so access currently given only to Koumbit sysadmins (see todo).

guerin.indymedia.org
--------------------

* Montreal
* Indymedia Germany at main Koumbit colo (rocker.indymedia.org)
* Xen domU
* Charybdis
* Note: *phased out* (taken out of the network) and replaced by "chat1.koumbit.net". Used to be running services, not anymore.

chat1.koumbit.net
-----------------

* Montreal
* Koumbit (main point of presence)
* Xen domU
* Charybdis
* Atheme-Services
