A "cloak" is a string of text that will show to other people on the network if you are logged in to your user instead of your hostname.

# Requirements (e.g. you need to do this before getting a cloak)

1. if not already done, register with nickserv: `/msg nickserv register <newirconlypassword>` (you need to replace `<newirconlypassword>` by a good password you made up)
1. login to your user. You may want to configure your client to do this every time you login to the network. most clients have mechanisms to perform the login for you. You can also login manually by communicating with nickserv: `/msg nickserv identify <nickname> <yourircpassword>` (again, you need to replace `<nickname>` by your actual nickname, and `<yourircpassword>` with the password you set for your account).

# Grab the generic cloak by yourself

There is one cloak pattern that you can configure for your login by yourself. The generic cloak is @$account.unaffiliated@. To get it, you can talk with the @hostserv@ service:

1. `/msg hostserv take $account.unaffiliated` -- please note that the
   `$account` part should *not* be replaced by your account name. The
   `hostserv` service will substitute it automatically for you, and it
   is part of the cloak pattern.

# To get a cloak of your choosing

If the generic cloak doesn't suit your taste or needs, you can request one of your choosing:

* decide what string of text should be your cloak. when your cloak is
  set, you will show up on the network as <nickname>@<yourcloak> --
  only when logged in, however.
  
  Cloak names can contain numbers, letters, dots and hyphens. They
  must start and end with either a digit or a letter.
  
1. once logged in, say in the support channel (@#ircd@): I would like
   a cloak: `<the cloak name>`

1. wait, we will add it when we can
