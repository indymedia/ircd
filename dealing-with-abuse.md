The network is hooked into [dronebl.org->https://dronebl.org/] which keeps away some spammers, but obviously not all. Sometimes it's necessary to kick people out of the network when they don't play by the rule. We don't have formal rules on how to handle abuse, but in general we don't directly meddle in channel politics, except when users try to circumvent channel bans. Obviously, we sometimes have to resolve conflicts between channel operators or add channel operators when there are no operators at all.

[[_TOC_]]

# Channel operator resources

Very often, channel operators do not need network operators to deal with abuse, but might not know all the tricks they have at their disposal, so it's useful to note them here again. A lot of this comes from the [channel modes documentation->https://charybdis.readthedocs.io/en/release-3.5/cmodes.html]. Basic settings include:

* `+n` and `+t`: no messages from users outside the channel and topic modifiable only by channel operators
* `+s`: secret channel. it will not be visible in the server listings or `/WHOIS` for a user. surprisingly effective
* `+r`: only registered users can join. very effective, but might not be possible on public support channels as registration is a big barrier for entry on IRC
* `+S`: SSL-only, surprisingly effective: some bots do not bother connecting over TLS
* `+b $HOST`: ban the user from the channel.
* `+q $HOST`: like ban, but the user is not kicked out and is just silenced. useful for trolls as they don't know they are censored.
* `+i`: make channel invite only
* `+O`: make channel IRC operator only
* `+m` or `+z`: moderated channels

All those require users to be operators in the channel. A network operator can get channel operator rights in any channel with this:

```
/MSG OperServ MODE #channel +o user
```

See the other [channel modes->https://charybdis.readthedocs.io/en/latest/cmodes.html] you can use in the charybdis documentation, although that list is incomplete: the authoritative list is visible by typing `/QUOTE HELP CMODE` in your IRC client.

# Private message spam

Users susceptible to PM spam can set the `+R` mode on their nick to ignore messages from unregistered users.

```
/mode +R user
```

# Killing users

The basic way to ban a user network wide is to set a `AKILL` "line", which blocks a user based on its IP. The simplest way to do this is to talk with `OperServ` which deploys the ban on all servers for us, with a command like this:

```
/MSG OperServ AKILL ADD *@*.compute.amazonaws.com !T 7d weird freenode spam
```

In this extreme case, we blocked all of Amazon from connecting to the servers because of some spam coming from multiple AWS servers. A less brutal case is to block a single user:

```
/MSG OperServ AKILL ADD baduser !T 7d harassing users and avoiding channel bans
```

Notice how we set a temporary ban (`!T`) of limited duration (seven days, `7d`) and specify a reason (`harassing users and avoiding channel bans`). It's nice to put a "reason" that will make sense to other operators (including yourself) in the future when we review those bans.

A permanent ban can be set with `!P`. 

You can also look for users joining the network with the `+c` server notice masks, for example:

```
/MODE user +s +c
```

There are more such flags that can be useful, but too many to detail here, see [the full list of flags->https://charybdis.readthedocs.io/en/release-3.5/umodes.html#meanings-of-server-notice-masks] for more information.

# Host/nick pattern-matching

Sometimes the above is not enough and users return under a different IP address. In that case, it might be useful to match certain patterns like username or nickname, which can be done with the `RWATCH` command. For example, this bans the nick `psycl0n` from the network completely, regardless of the hostname:

```
/MSG OperServ RWATCH ADD /^psycl0n!/iSK troll, death threats
```

Some explanation:

* `/^psycl0n!/` is a regular expression.
* `/` marks the beginning and end of the regex, like in Perl
* `^` is the beginning of the host string, like in Perl
* `!` is the hostmask seperator (which separates the IRC name from the hostname)
* `i` means case-insensitive, like in Perl
* `S` means to show the nick in the `#services` channel
* and, finally, `K` means to kill the darn user

Those patterns should be tested with `RMATCH` first to make sure only the right user is banned.

# Killing users joining a channel

Botnet frequently try to grab a hold on a network by rejoining always a control channel. Once found, this channel can be blocked by first registering it with `ChanServ` then telling `OperServ` to automatically kill users joining a certain channel:

```
/MSG ChanServ REGISTER #channel
/MSG OpeServ KLINECHAN #channel ON reason
```

Anyone that joins `#channel` will be temporarily killed on the network, which is very effective. Already present users will not be killed, but might if they disconnect and reconnect.

# Other approaches

There's a [irssi script->https://dronebl.org/downloads/dronebl.pl] to interoperate directly with DroneBL. It can list and add entries to the database which can be useful for other networks, but wasn't tested by the staff here (yet?).

Freenode uses a [Linmoria->https://github.com/ProgVal/Limnoria] bot called [Sigyn->https://github.com/freenode/Sigyn] for some of that management. It can kill users based on regular expressions on things said in channels, on repetition and all sorts of neat stuff. 

Atheme has a bot to censor "bad words" called [cs_badwords.c->https://github.com/atheme/atheme-contrib-modules] that could be useful to channel operators as well. It should be available in our version of atheme and might just be a matter of enabling the module and configuring it...

# Server-wide announces

When trouble comes, it's good to warn users something is going on. The typical way to do this is to send a Global Notice. This can be done by talking with the `Global` service:

```
/msg Global This is a test. Do not try this at home.
/msg Global SEND
```

If services are down, however, this will not work and you'll need to issue a notice by hand, using a special `/notice` command, targeting all server domains:

```
/notice $$*.koumbit.net This is a test. Do not try this at home.
/notice $$*.indymedia.org This is a test. Do not try this at home.
```

Note how you need to cycle through all the domain names of the servers in the network (see the list with `/links` or `/map`).

In both cases, you obviously need to be a network operator on the servers.
