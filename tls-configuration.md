# Switch to DNS-based authentication

On April 26 2020, certbot was configured to do a DNS-01 challenge with Let's Encrypt and the [dns-rfc2136 plugin](https://certbot-dns-rfc2136.readthedocs.io/en/stable/) on all three servers. They now have a valid TLS certificate.

The configuration is that every server fetches its own version of the irc.indymedia.org TLS certificate using DNS-01 authentication. Ideally, each server would also add its own FQDN to the cert, but that still needs to be configured. We needed a patched version of certbot to make this work.

Then RFC2136 "nsupdate" keys are distributed from the primary DNS server to each ircd. Those keys have UPDATE access on the `irc-le.indymedia.org` zone and generate their own cert in their own time. We use the `--reuse-key` parameter to make sure the private key doesn't change on renewal, as we use the private key fingerprint in the cross-connect configuration.

Changes performed:

1. chat0.koumbit.net relinked with chat1.koumbit.net
1. chat0.koumbit.net private key changed
1. irc.indymedia.org cert created on chat0.koumbit.net with DNS-01 authentication, with virtualenv code that will fail at next renewal
1. DNS-01 configuration performed on chat0.koumbit.net
1. chat0 and che added back into rotation (che still down)
1. DNS-01 configuration attempted (and failed) on che

To reproduce the config:

1. deploy certbot from the fork:

        git clone https://github.com/certbot/certbot/
        cd certbot
        git remote add -f hpax https://github.com/hpax/certbot.git
        git checkout hpa/rfc-2136-cname-fix
        apt install python3-venv
        python3 -m venv ~/.virtualenv/certbot
        cd certbot
        ~/.virtualenv/certbot/bin/pip3 install .
        cd ../certbot-dns-rfc2136
        ~/.virtualenv/certbot/bin/pip3 install .

1. configure a `/etc/letsencrypt/rfc2136-credentials.ini` file with the following content (e.g. on che), the `dns_rfc2136_name`, `dns_rfc2136_secret` and `dns_rfc2136_algorithm` parameters are magic and come from the DNS primary :

        # Target DNS server
        #dns_rfc2136_server = ns2.riseup.net.
        # https://github.com/certbot/certbot/issues/7871
        dns_rfc2136_server = 204.13.164.8
        # Target DNS port
        dns_rfc2136_port = 53
        # TSIG key name
        dns_rfc2136_name = che_ircd_key
        # TSIG key secret
        dns_rfc2136_secret = [CENSORED]==
        # TSIG key algorithm
        dns_rfc2136_algorithm = HMAC-SHA512

1. create the certificate:

        ~/.virtualenv/certbot/bin/certbot --dns-rfc2136-credentials /etc/letsencrypt/rfc2136-credentials.ini  certonly -d irc.indymedia.org --register-unsafely-without-email --dns-rfc2136 --reuse-key

1. change permissions on the generated cert:

        chown -R :ssl-cert /etc/letsencrypt/archive/
        chmod -R g+rX /etc/letsencrypt/archive/

1. add the cert to `/etc/charybdis/ircd.conf` and restart the ircd

        service charybdis restart

1. extract the new SHA256 private key fingerprint:

        certtool --pubkey-info --load-privkey /etc/letsencrypt/live/irc.indymedia.org/privkey.pem |  grep -i sha256 | grep -v pin-

1. add that key to the `connect` block on the other servers, and restarted those ircds:

        service charybdis restart

1. create cronjob to use patched certbot for renewals:

        cat > /etc/cron.daily/certbot-rfc2136 <<EOF
        # We need to use our patched version of certbot to renew certs so that DNS-01
        # challenges can be completed

        SHELL=/bin/sh
        PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

        0 */12 * * * root /usr/local/bin/patched_certbot_renew
        EOF
        cat > /usr/local/bin/patched_certbot_renew <<EOF
        #!/bin/sh

        # Use the patched certbot to renew certificates
        /root/.virtualenv/certbot/bin/certbot -q renew
        EOF
        chmod a+x /usr/local/bin/patched_certbot_renew

1. create renewal hook so that charybdis takes on the new certificates automatically

        cat > /etc/letsencrypt/renewal-hooks/deploy/charybdis <<EOF
        #!/bin/sh

        /usr/sbin/service charybdis reload
        EOF
        chmod a+x /etc/letsencrypt/renewal-hooks/deploy/charybdis


To configure a DNS primary server, the following command need to be issued:

```
dnssec-keygen -a HMAC-SHA512 -b 512 -n _acme-challenge.irc-le.indymedia.org chat1.koumbit.net.
dnssec-keygen -a HMAC-SHA512 -b 512 -n _acme-challenge.irc-le.indymedia.org chat0.koumbit.net.
dnssec-keygen -a HMAC-SHA512 -b 512 -n _acme-challenge.irc-le.indymedia.org che.indymedia.org.
```

And the following config must be added to bind:

```
key "chat1.koumbit.net." {
  algorithm hmac-sha512;
  secret "[KEY CONTENT]";
};

key "chat0.koumbit.net." {
  algorithm hmac-sha512;
  secret "[KEY CONTENT]";
};

key "che.indymedia.org." {
  algorithm hmac-sha512;
  secret "[KEY CONTENT]";
};

zone "irc-le.indymedia.org." IN {
  type master;
  file "irc-le.indymedia.org";
  update-policy {
    grant chat1.koumbit.net. name _acme-challenge.irc-le.indymedia.org. txt;
    grant chat0.koumbit.net. name _acme-challenge.irc-le.indymedia.org. txt;
    grant che.indymedia.org. name _acme-challenge.irc-le.indymedia.org. txt;
  };
};
```

Then the challenge record gets delegated with this entry in the `indymedia.org` zone:

```
_acme-challenge.irc.indymedia.org CNAME _acme-challenge.irc-le.indymedia.org.
```

# Problems encountered

1. che blocks responses from primary nameserver (ns2.r.n), we see the traffic goign ouit of ns2 but not in che. this makes it impossible for che to get its own certificate, so we gave up on it for now.
1. certbot doesn't take a hostname as a DNS server argument: https://github.com/certbot/certbot/issues/7871 workaround: we used the IP
1. _acme-challenge CNAME delegation is buggy in certbot. workaround: we deployed code from: https://github.com/certbot/certbot/pull/7244
1. the dns-rfc2136 plugin hardcodes a DNS server instead of looking up the NS records, which makes it impossible to (say) authenticate with `koumbit.net` *and* `indymedia.org`, which makes it impossible to have a `SubjectAltName`  for the `koumbit.net` servers
1. when the certbot plugin failed, we tried to use dehydrated but we couldn't figure out how to make it work. it might be possible to make a custom renewal hook to implement exactly what we need here and that might be simpler than patching certbot!

# Remaining work

1. -contact che people re networking troubles, and redo the DNS-01 deployment on che-: DONE! DNS updates now work. we investigated why certbot couldn't fallback to TCP (which works, as confirmed by `nsupdate -v`) but could not figure it out
1. deploy CNAME delegation workaround "correctly": right now renewal will fail on chat0 because certbot is deploy from `~/.virtualenv/certbot` and not in the system's `$PATH`. this might involve forking in a different plugin
1. -a new cert needs to be issued on chat1.koumbit.net, and the fingerprint distributed to the other nodes, using the DNS-01 mechanism-: DONE! chat1 and che now both have valid certs too, although not with their own name.
1. research how it would be possible to add the `SubjectAltName` for the `koumbit.net` servers. would probably need a custom dehydrated hook, or more patches to certbot.
** this does not seem to be necessary
1. -the charybdis servers probably need to be restarted in the certbot hooks, which is not currently the case-

# Legacy setup - Initial problematic

Before april 2020, our TLS configuration was this: `chat1.koumbit.net` was generating the cert which we were copying by hand to all hosts. But at some point in 2020, this completely stopped working, because of [changes in Let's Encrypt](https://letsencrypt.org/caaproblem/). So other servers were kicked out of rotation and the cert was correctly renewed.

