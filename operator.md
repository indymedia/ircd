# Operating the IRC network

[[_TOC_]]

## What is an IRC Operator?

Being an IRC Operator means that you have quite a bit of power of the IMC
IRC network, this power is not glamorous, it can be difficult to deal with
at times. Please understand that being an IRC Operator does not mean that
you can abuse your power, you should not kill or kick off anyone from any
channel or the server who you disagree with or you don't like. Being an IRC
Operator means that you do some cleanup, you sometimes have to mediate in
disputes, you provide help and point people towards resources, you should be
polite and patient with people and most importantly, be there to help when
it is needed. If you want to be an IRC Operator because it is cool, or you
enjoy power, then forget it, the point is to help out and spread work around
so it is light for everyone.

IRC Operators are not police, you can't stop illegal activity and are not
responsible for any that happens on the server, you are an operator not a cop.

An IRC Operator is identified by most IRC clients when you do a /who * in a
channel they are in and you will see a `*` by their name (`@` is a channel
operator, server operators can also be channel operators, these people will
have `*@` next to their name). Also doing a `/whois <nick>` will show you that
this person is an operator:

```
00:06 -!- PseudoPunk [irc@localhost]
00:06 -!-  ircname  : grrrr
00:06 -!-  server   : kropotkin.indymedia.org [kropotkin IMC IRC server]
00:06 -!-           : IRC Operator
00:06 -!- PseudoPunk is a Server Administrator
```

## What is required of an IRC Operator?

IRC Operators __must__ have a registered nick with nickserv, be connected to the
network via TLS (no plain text connection!), be present in an `oper block` in
the network configuration, be subscribed to the ircd@indymedia.org mailing list,
have ssh access with admin privileges to all network machines, hang out in
`#ircd` when connected to the network, help out with irc issues and most
importantly not abuse their power.

IRC Operators __should__ know the services nickserv, chanserv, and operserv
(knowing memoserv, statserv, etc. is good too, but isn't necessary). These
are easy to get to know if you spend some time doing `/msg nickserv help`,
`/msg chanserv help`, etc.

## How to become an IRC Operator

As said earlier, there is a lot of power associated with being an IRC
Operator, unfortunately we need to know that you are going to be able to act
like a benevolent dictator and not a tyrant before we are going to give you
the keys to one of our most important communications mechanisms. Participate
in meetings, answer questions on imc-tech or one of the other tech
listservs, help people out in #tech, or work closely on one of the active
coding projects. Demonstrate to others that we can trust you, and you will
find that you will be welcomed with open arms.

## Access management

### How to create a new IRC Operator

This needs to be done by an existing operator for the new person.

1. Create a linux user on all of the network hosts and set a strong temporary
   password for the account -- note down that password. Add the user to the
   `ircop` group. Install they ssh key(s) in the new user's `authorized_keys`
   file -- all remote access happens only by ssh key.

   Send the temporary password to the new operator using a pgp-encrypted email.
   Check with them that they are able to login, that they could change their
   account password with the one you sent to them, and finally that they can
   sudo to root with their newly set password.

   Finally, make them clone the `sharedconf` git repository with a remote for
   each network host. See [sharedconf](sharedconf)

2. Have the new operator create their own `operator` block by following the rest
   of this point (or add it yourself if you know the required hostmasks):

   In the `sharedconf` git repository, edit the file `opers.conf`. Each
   operator should have one `operator` block in there and they should look
   like the following:

   ```
   operator "micah" {
           user = "*@*127.0.0.1";
           password = "A_PASSWORD_HASH_GOES_HERE";
           snomask = "+Zbfkrsuy";
           privset = "admin";
   };
   ```

   Copy an existing block, paste it in and then make the appropriate changes:
   change the name, the hostmask in the user line and the password hash.
   Note that you can have multiple user lines to permit yourself operator
   connections from more than one host.

   To generate a password, ssh to one of the network machines and use:

   ```
   charybdis-mkpasswd -s $(head -c 12 /dev/urandom | base64 -)
   ```

   Save the file and commit it to git. Push your change to all network
   machines.

3. Rehash the config file in IRC by typing `/rehash` while you are an IRC Operator.

4. See if the new operator can type `/oper`, type their password, and then
   become an operator.

5. On the server which hosts the services (as of this writing, it is chat1),
   edit `/etc/atheme/atheme.conf` to add in the new person. Towards the end of
   the file, you'll find `operator` blocks. Each network admin should have one.
   Copy one of the blocks and change the user name. It should look like the
   following:

   ```
   operator "micah" {
        operclass = "sra";
   };
   ```

   Request a rehash of the services config file. Someone that already has access
   to services as an operator can issue `/msg operserv rehash`, or if you're
   logged in to the host that runs atheme services, you can issue `service
   atheme-services reload`.

   Have the new user look at `/msg operserv help` to see if the listing contains
   operator command slike `REHASH`.

6. Grant them full access flags to the `#ircd` and `#services` channels with
   `/msg chanserv #ircd +AORVefiorstv` (same for other channel).

   Verify with the new operator that they can join `#services` once they've
   identified as an operator to the network with `/oper`.

7. On 0xacab.org, invite the person's user (have them create one first if they
   don't have one) to be a member of the `ircd` project and set their permission
   as `owner`.

   Instruct the new operator to clone the documentation git repository to have a
   local copy.

7. Finally, send an announcement to ircd@lists.indymedia.org to let everyone
   know that there is a new operator on board.

### How to remove an operator

1. ssh to all network hosts and remove their local user. Make sure that it's not
   being brought back by puppet.

2. On the host running the services (currently chat1), edit
   `/etc/atheme/atheme.conf` and find the `operator` block for the user and
   remove it. Save the file and then request a rehash with `service
   atheme-services reload`.

3. in the sharedconf git repository, edit the file `opers.conf` and remove the
   person's `operator` block. Then identify with operserv and rehash the
   configuration with `/msg operserv rehash`.

4. remove permission flags for the user on special channels `#ircd` and
   `#services` with `/msg chanserv flags #ircd <user> -AFORVefiorstv` (and same
   for the other channel). If necessary, kick them out of the private channels
   restricted to operators -- `#ircd` is public so they can definitely continue
   lurking there as they want.

5. on 0xacab.org remove their user from members of the `ircd` project.

6. revoke their membership and remove their pgp key from the
   ircd@lists.indymedia.org mailing list.

7. send an announcement of the operator access removal to
   ircd@lists.indymedia.org

## IRC Operator duties

### Securing a channel's operator status

Sometimes channels lose their channel operators, due to lost connections, or
whatever, and the only way they can get them back is by making everyone
leave the channel and then come back in. This isn't feasible if people are
idled, so you may be asked to come and secure channel operator status.

If you know someone on the channel, you can give them channel operator
status through following these instructions, but if nobody is on the channel
that you know, it would be wise to pay attention to the channel to figure
out who should have access.

To return channel operator status to a channel follow the steps in (NOTE: dead
link) <http://docs.indymedia.org/view/Sysadmin/IrcD#Getting_OP_privileges>

If you need to give someone else operator access in the channel after it has
been secured do so through `/mode #channel +o <nick>`.

Once you have secured a channel, try to instruct the people there about
chanserv and how to use it so that they will always have operator access on
the channel.

### Services are unavailble, how to reconnect them

NOTE: the text in this section seems to be deprecated. judi is not a network
host anymore and we don't use hybrid services anymore. atheme services seem to
be more reliable with regards to reconnecting automatically. There is still some
information that we may want to keep around. We should rewrite this section.

The services are down, the server which runs them is not connected
to the network. At the moment, that server is judi. We have attempted to set
this server up to autoconnect after it drops off, but this isn't exactly
reliable, so if you find yourself in a situation where services aren't
available, then you will need to reconnect the link to the services node.

To reconnect a server do: /quote connect judi.indymedia.org (some clients
don't need the quote and you can just do /connect), this can only be done
when you are an IRC Operator.

Once it comes back online, you should get messages from nickserv if your
nick is registered.

What to do if this doesn't work? Generally this means that the services on
the services server need to be restarted. To do this, follow the directions
in (NOTE: dead link)
<http://docs.indymedia.org/view/Sysadmin/IrcD#Restarting_Hybrid_Services>

### How to deal with bad users

Sometimes people show up on channels to cause problems, flooding the channel
with nick changes, obscenities, or whatever. This is not tolerated here,
those people will be kicked and banned from the server.

What I think is the best way to deal with someone like this is to take it in
steps, sometimes steps have to be skipped if the person seems to be totally
out of control:

1. Talk with the person (if you can), and tell them that you are an IRC Operator
   and that they will be removed from the server forcibly if they don't stop
   their behavior.

2. Kick them from the channel that they are causing problems with (`/kick <nick>
   reason`), if they keep coming back, ban them from the channel (`/kickban
   <nick> reason` -- this depends on your client, you might need to issue a
   `/ban` and then a `/kick` separately, or there might be a more convenient
   alias for this). For example:

   ```
   /BAN looser       - Bans the nick 'looser'
   /BAN -host looser - Bans the host of nick 'looser'
   /BAN *!*@*.org    - Bans all the users coming from any .org domain (don't do this)
   ```

   Be very careful that you don't ban the `*@localhost` address or you are going
   to keep out a lot of people (e.g. all tor-connected users)...

3. If they keep causing problems, kick them from the server (use operserv's
   command `/msg operserv kill <nick> reason`)

4. If they keep coming on and causing problems, they will have to be kicked and
   banned from the server. (use operserv's `/msg operserv gline <user@host>
   [reason]`) -- again, **DON'T** gline irc@localhost!! Ban them for something
   like an hour and a half (see second example below):

   ```
   Examples:

   # Creates a permanent gline for *@*.lamers.org.
   /msg operserv GLINE *@*.lamers.org  Don't come back

   # Glines *clone@clone.com for an hour and a half, with the message "no
   # clones"
   /msg operserv GLINE 1h30m *clone@clone.com no clones
   ```

### How to send a global notice

Sometimes you need to alert everyone on IRC that things need to be
restarted, or whatever, this should be done for technical reasons only!

Use: `/notice $$*.indymedia.org` to hit all servers or `/notice
$$judi.indymedia.org` for just that server

### Cloaks

The NickServ service provides a default cloak templates that users can
self-request. See
[user cloak documentation](getting-a-cloak#grab-the-generic-cloak-by-yourself).

To provide arbitrary cloaks for users, you can use the following commands:

```
/msg NickServ vhost <nick> [<host>]
```

For example, to show myself as anarcat@koumbit.org, I used:

```
/msg NickServ vhost anarcat koumbit.org
```
