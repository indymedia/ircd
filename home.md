The homepage of this wiki is empty because of a rather [silly bug in GitLab that makes it impossible for the homepage name to be changed to something else](https://gitlab.com/gitlab-org/gitlab/-/issues/19842) (or, inversely, [for a repository to use `home.md` as the preview for a directory](https://gitlab.com/gitlab-org/gitlab/-/issues/432344)).

See the [README.md](README) file for the proper landing page.
