Discussion:

 * we want to simplify
 * we agree on 3 machines, at least one out of koumbit
 * we'd like to have redundancy or at least a possible fallback of services or backup.
 * we have a broken backup server that we could fix - stefani has a machine for this.
 * in the long run, we think switching to another ircd software would be a good thing. contenders would be inspircd and freenode's ircd-seven
   * inspircd was apparently hostile to the last security fix/patch
   * ircd-seven is currently not packaged in debian

status:

 * chat1 - currently the only node in production. holds services, primary koumbit DC (netelligent/eStruxture, montreal), ONLINE
 * chat0 - secondary koumbit DC (openface, montreal), ONLINE - running the older gnutls backport for some weird reason, will be updated with the new backport
 * che - (tachanka / mayfirst, NYC), OFFLINE - stuck at crypto key

battle plan:

 * upgrade che and chat0 to stretch
 * make sure charybdis+mbedtls still works after the upgrade - it does
 * fix backups
