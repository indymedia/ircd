The [Charybdis 4.1 upgrade->https://we.riseup.net/ircd/charybdis-4-1-upgrade] imposed some changes on the TLS configuration of the servers which change the way public certificates are fingerprinted across the networks. This was changed to fix inter-server authentication but also affects @CERTFP@ authentication with @NickServ@ which means users authenticating with this mechanism now will need to add a new fingerprint to be able to authenticate in the future.

h1. How to add your new fingerprint

Because the servers are not currently setup using the new mechanism, you will need to compute the certificate by hand. This can be done with the @certtool@ command that is part of the [GnuTLS->https://www.gnutls.org/] distribution (on Debian, it is in the @gnutls-bin@ package), for example with:

<code>
$ certtool --pubkey-info --load-privkey .irssi/certs/nickserv.privkey | grep sha256 | sed 's/.*sha256:/SPKI:SHA2-256:/'                                                                                                                SPKI:SHA2-256:b89943391739daadf89e191575307b590b5db40f1b6a52cbe39f2ea62cb16637
</code>

Note that only the more recent versions of GnuTLS give out SHA256 fingerprints. The above works on Debian stable with GnuTLS 3.5. If you are running older releases of GnuTLS (for example 3.4 only gives out a SHA1 checksum), you might need to use OpenSSL instead, for example:

<code>openssl pkey -inform PEM -pubout  -outform DER < .irssi/certs/nickserv.privkey | sha256sum</code>

This fingerprint can then be added to the configuration through @NickServ@ with:

<code>
/msg NickServ cert add SPKI:SHA2-256:b89943391739daadf89e191575307b590b5db40f1b6a52cbe39f2ea62cb16637
</code>

And you're done! Alternatively, you can just wait for the authentication to fail and then authenticate with your password and use this to add the new certificate:

<code>
/msg NickServ cert add
</code>

The above will naturally not work until we have performed the migration to the new fingerprint mechanism.

The certificate fingerprint mechanism migration will occur in two weeks from publication of this notice, before the certification expiry date (2018-07-03). It can be done without upgrading to 4.1 so it's not because a server is running version 4.1 that it uses the new mechanism, nor that a 3.5 server means it can't use the new mechanism either.

h1. How to configure CERTFP authentication

Even if you do not currently use CERTFP to authenticate with the IRC server, you might want to consider adding that to your configuration anyways. It makes it possible to avoid storing cleartext passwords in your IRC configuration files and is generally considered more secure against bruteforce attack than password-based authentication. Even more so if you do not store the password in your configuration files: then you don't have to remember the password and can use a much stronger one.

We have [previously->https://we.riseup.net/ircd/guerin+certfp] suggested following the [OFTC guide->https://www.oftc.net/NickServ/CertFP/] to configure CERTFP, but when the new changes become live in two weeks, the configuration will be even simpler. All you need to do is generate a private key and add it to your IRC client's configuration. This procedure here is an example using @certtool@ and @irssi@.

First generate a RSA private key:

<code>(umask 077; certtool --generate-privkey --rsa | sed -n '/^-----BEGIN/,$p' > .irssi/certs/nickserv.privkey)</code>

Then generate the fingerprint to send to @NickServ@:

<code>certtool --pubkey-info --load-privkey .irssi/certs/nickserv.privkey | grep sha256 | sed 's/.*sha256:/SPKI:SHA2-256:/'</code>

or with OpenSSL:

<code>openssl pkey -inform PEM -pubout  -outform DER < .irssi/certs/nickserv.privkey | sha256sum</code>

Send it to @NickServ@:

<code>/msg NickServ cert add SPKI:SHA2-256:<...></code>

And add the certificate to your configuration, for example:

<code>
  {
    address = "irc.indymedia.org";
    chatnet = "IMC";
    port = "6697";
    use_ssl = "yes";
    ssl_cert = "~/.irssi/certs/nickserv.privkey";
    ssl_verify = "yes";
    autoconnect = "yes";
  },
</code>

The latter configuration and the path to the certificate file will change according to your IRC client configuration.

h1. Problems?

If you have any problems with this, come and chat with us in the @#ircd@ channel or in private with an oper (see @/stats p@ for a list), as usual.

update, 2018-06-20:

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

I, anarcat, certify there is a configuration change required on the
CERTFP configuration of clients of the irc.indymedia.org network, to
be performed between 2018-06-20 and 2018-07-03.

The change consists of changing the fingerprint from the public
certificate to the RSA public key. The resulting fingerprint should be
extracted with:

certtool --pubkey-info --load-privkey .irssi/certs/nickserv.privkey | grep sha256 | sed 's/.*sha256:/SPKI:SHA2-256:/'

This message should be signed with an OpenPGP key that you can verify
through Debian's keyrings.

More details are available in this announcement:

https://we.riseup.net/ircd/certificate-authentication-requires+467154
-----BEGIN PGP SIGNATURE-----

iQEzBAEBCAAdFiEEexZCBNCWcjsBljWrPqHd3bJh2XsFAlsqY0UACgkQPqHd3bJh
2XsFJQf+NUsqwbx0So4acoPE6ZEYUbR9LTv+gfkfYlrhEmzz1rTtFLtCqFA3Yphd
FXha7iT6hUJTY0Z5Led+dr9qBZNfBUzCWAoehSb5ZA8QKgaGHyo9fE1ZkXjXIHDF
+8lClp+oV9J7LZS+U09dsANruwUacarDWYP/dSyuy0i7Q9CSr/Svx6UuQEO0NGI/
Qd6dZ+wra412c3wjEh/1bmS63+CTzWzB9en6wy2S4yfb1pfgKprlzRsd9J2sRXj0
poqlu6S9eCDoy6owbKxfIIUTE5/cPnA1pWV0V2qnkZfYzVaHIIzj7w7091Ep8wAx
8Lica9I9dVLIBqCr44TKCjBwL0veoA==
=VXuk
-----END PGP SIGNATURE-----
```

update, 2018-06-22:

For irc.koumbit.net:

```
 * Certification info:
Subject:
     CN=irc.indymedia.org
Issuer:
     C=US
     O=Let's Encrypt
     CN=Let's Encrypt Authority X3
Public key algorithm: rsaEncryption (2048 bits)
   Sign algorithm sha256WithRSAEncryption
   Valid since Apr  4 15:38:01 2018 GM to Jul  3 15:38:01 2018 GM
* Cipher info:
   Version: TLSv1.2, cipher ECDHE-RSA-AES256-GCM-SHA384 (256 bits)
```

This server uses only GnuTLS (Version TLSv1.2). You may consider with using or add this configuration for irc client:

```
  {
    address = "irc.indymedia.org";
    chatnet = "IMC";
    port = "6697";
    use_tls = "yes";
    tls_cert = "~/.irssi/certs/nickserv.privkey";
    tls_verify = "yes";
    autoconnect = "yes";
  },
```

update, 2018-07-20:

The `certfp_method` was changed today successfully. The older keys can be removed from nickserv, but it’s harmless to keep them there as well.
