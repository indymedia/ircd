Dump of https://we.riseup.net/ircd/todo

this is incomplete because each of those tasks have a "hidden"
description that pops up when you click on each link, and that is a
real pain in the ass to move out.

besides, those tasks are *old* and we are making a fresh start in:

https://0xacab.org/indymedia/ircd/-/issues

Tâches en attente

    configuration management: how to share the "shared confs"
    add a new node outside of mayfirst or koumbit's POPs
    review atheme's help files • anarcat
    user guide
    coordination with upstreams
    tor HS redundancy (needs testing) • lelutin
    Add secure connection howto and reference it in motd
    fix backups! the configs dont need it, but the vairous dbs should be backed up - use khalo? • Micah
    clarify goal and name
    automate let's encrypt deployments • anarcat, Micah 

Tâches terminées

    decide what to do with guerin • anarcat
    upgrade atheme to 7.0.7 • anarcat
    add procedures for fallback of network services • anarcat
    backups of services • anarcat
    move documentation from the old docs website here • anarcat
    make sure atheme doesn't log IPs • anarcat
    upgrade chat0 to jessie • lelutin
    start using LE for chat.koumbit.net and remove K's cert from chat0 • lelutin
    grant SSH access to other IRCops to chat0 • lelutin
    upgrade chat1 to stretch • lelutin
    fix contact email / discussion list? (ircd@lists.i.o down) • Micah
    use let's encrypt for all certificates
    consider using hostserv to allow users to register their own ghosts • lelutin 

 
anarcat
06/04/2016
	
Éditer

for now, i will assume that guerin will be dismantled, and i am building an alternative on chat1.koumbit.net, and documenting everything here.
 
 
anarcat
06/04/2016
	
Éditer

services documentation is in we.riseup.net/ircd/services
 
 
anarcat
06/04/2016
	
Éditer

atheme running 7.0.7 on chat1.koumbit.net.

i’ve created a task for running backups and marked the fallback procedure as closed because we.riseup.net/ircd/services should cover most of it for now.
 
 
anarcat
06/04/2016
	
Éditer

backups running at koumbit. note that primary backups are accessible from the host, so a host compromise can also compromise the backups, but that allows us to access them as well, see motd. koumbit has offsite backups of those primary backups to recover in case of host compromise, as support@Koumbit.org if necessary.
 
 
anarcat
06/04/2016
	
Éditer

i have migrated some docs from the old wiki here:

    docs.indymedia.org/Sysadmin/IrcdNetwork – mostly a rewrite, in we.riseup.net/ircd/servers
    docs.indymedia.org/Sysadmin/IrcdProcess – simply dropped to the ground and made another todo here to figure out what the heck is our process anyways
    docs.indymedia.org/Sysadmin/IrcDMeetings – ignored
    docs.indymedia.org/Sysadmin/SecureIRC – ignored, probably totally outdated. would be good to created a user guide for secure-only channels and tor
    docs.indymedia.org/Sysadmin/IrcdSetup – totally outdated, needs to be documented in we.riseup.net/ircd/servers
    docs.indymedia.org/Sysadmin/IrcHowTo – needs a total review and overall, not imported – would need to be public anyways
    docs.indymedia.org/Sysadmin/IrcAttack – similar – should be on a public site too

That about covers the old docs, i think we can mark that as resolved, since i don’t think any user was really using those docs (they were not linked anywhere).
 
 
anarcat
07/04/2016
	
Éditer

looks like logging is really fixed.
 
 
anarcat
12/04/2016
	
Éditer

had a good conversation with Myon on #oftc regarding their Let’s Encrypt setup. details in the above todo.
 
 
anarcat
14/04/2016
	
Éditer

note: i have documented the current configuration management process in [sharedconf], along with the known problems and possible alternatives.
 
   
lelutin
05/03/2018
	

I tried enabling the tor HS on all nodes and it didn’t go quite as expected. I’ve added some details to the todo point.
 
 
anarcat
	
Aide de Mise en Page
 

