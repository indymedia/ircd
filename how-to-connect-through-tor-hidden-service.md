You can connect to Indymedia IRC server through a Tor Hidden Service, instead of just using Tor to connect to the normal `irc.indymedia.org` address (which is also possible, by the way).

The hidden service has the address `akeyxc6hie26nlfylwiuyuf3a4tdwt4os7wiz3fsafijpvbgrkrzx2qd.onion`.

If your tor software is too old and can't connect to the previous address, you can use the following address: `h7gf2ha3hefoj5ls.onion` -- however, if it is possible to upgrade your tor software to a more recent version we strongly recommend that you do this and use the first address mentioned above.

You should connect to it *without SSL* on *port 6667*. The SSL/TLS is already present in Tor, so it's not necessary here.

Warning: SSL/TLS connections are currently allowed through the hidden service (only the "old" v2 onion service), however the service will present to you a certificate that is only valid for host name `irc.indymedia.org`, thus not for the onion host name. Because of this we recommend not to use SSL/TLS with the hidden service since it's impossible to trust the certificate in this situation.
