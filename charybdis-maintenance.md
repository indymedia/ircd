To build charybdis from source:

<code>
git clone git://anonscm.debian.org/collab-maint/charybdis.git
cd charybdis
gbp buildpackage
</code>

This will build the package on the default branch, that is for unstable. This may not be what you want to deploy in production. For that, you will need to build for jessie, or whatever the hosts are currently running. The following assumes you are using [sbuild->https://wiki.debian.org/sbuild] and that you want to link against mbedtls:

<code>
env DEB_TLS_LIB=mbedtls gbp buildpackage --git-debian-branch=debian-jessie-backports-sloppy --git-builder=sbuild --add-depends='libmbedtls-dev' -c jessie-amd64-sbuild --extra-repository='deb http://cdn.debian.net/debian jessie-backports main contrib non-free'
</code>
