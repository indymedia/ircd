There are many webchat software to provide access to the IRC servers through a regular web browser, something which can be useful for occasional users. Some collectives run webchats that interface with the irc network, here are the currently known interfaces:

* https://chat.koumbit.net/
* https://irc.indymedia.nl/

Software options
=============

QwebIRC
-------------

Currently running on Koumbit's machine, documentation here: https://wiki.koumbit.net/QwebIrc

 * based on Python and Twisted
 * requires some javascript on the client side
 * simple web interface without logging
 * no releases, no debian package
 * project still active

The Lounge
----------------

https://thelounge.github.io/

* web bouncer, with server-side logging
* responsive interface
* node.js
* multi-server support (or hardcoder)
* requires every user to be created by hand: https://thelounge.github.io/docs/server/users.html
* demo: https://avatar.playat.ch:1000/
* webirc only in 2.0+

Installing on Debian jessie:

```
sudo apt install npm nodejs-legacy
npm -g install thelounge
```

IRC anywhere
--------------------

http://ircanywhere.com

* web bouncer with backlog
* node.js
* irc bouncer planned
* seems abandoned, demo disabled: https://github.com/ircanywhere/ircanywhere/issues/278
* mongodb

Iris
-----

https://github.com/atheme-legacy/iris

qwebirc with some tweaks to talk to atheme-services. seems abandoned.

Subway
----------

https://github.com/thedjpetersen/subway

* Web bouncer
* abandoned?

KiwiIRC
-----------

http://kiwiirc.com

* multiple network support
* nodejs
* AGPL

RFP: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=646776
