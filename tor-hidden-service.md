# Configuration

We have a Hidden Service to allow Tor users to connect directly to the IRC network. The hostname is:

```akeyxc6hie26nlfylwiuyuf3a4tdwt4os7wiz3fsafijpvbgrkrzx2qd.onion```

It has a minimal configuration:

```
# relay
OutboundBindAddress 199.58.80.38
BandwidthRate 256 KB
BandwidthBurst 256 KB
ContactInfo imc ircd admins <ircd at lists dot indymedia .org>

# hidden service hidden_irc
HiddenServiceDir /var/lib/tor/hidden_irc
HiddenServicePort 6667 199.58.80.38:6667
# TLS connections are not working through tor, so it's questinable to even be
# listening to that port
HiddenServicePort 6697 199.58.80.38:6697

# hidden service hidden_irc_v3, e.g. with onion service v3
HiddenServiceDir /var/lib/tor/hidden_irc_v3
HiddenServiceVersion 3
HiddenServicePort 6667 199.58.80.38:6667

# exit policies: user_exit_policy
#default
#ExitPolicyRejectPrivate 1
ExitPolicy accept private:6697
ExitPolicy accept private:6667
ExitPolicy reject *:*

# allow tor-arm to work
DisableDebuggerAttachment 0

# All IRC nodes will be "fighting" to be the one to serve the HS. In order to
# make downtimes shorter when the current serving node goes down, we're pushing
# down the following value. When set lower, tor pushes the value up to 10mins
# so it seems to be the lower bound.
RendPostPeriod 10 minutes
```

Then a hidden service key is stored in `/var/lib/tor/hidden_irc` for the v2 onion service and in `/var/lib/tor/hidden_irc_v3` for the v3 onion service. This is configured by Puppet on che in `/root/puppet/manifests/site.pp` (not the v3 onion service).
Currently (2017-Sep-28) the onion service runs on chat1.koumbit

## Security considerations

The IP addresses are specified because otherwise tor users would be able to brute-force search the services password. Note that it is *still* possible for that brute-force searching to happen because the ExitPolicy is set to `accept private...` which allows a determined attacker to build his own circuit to connect to localhost here, even if the Hidden Service doesn't allow it explicitely. But at least it's somewhat hidden.

## Possibility of failover setup

This was setup on 2018-02-11 by LeLutin. We need to test that other nodes are picking up the HS when tor is out cold for 10mins. If this works out well then this section will be adjusted to explain how things work instead of how they *might be* working.

There is the possibility of distributing this setup to other hosts. All that would be needed would be to copy the above config on all IRC servers, along with the hidden service keys. According to dgoulet, this would make the various servers "fight" to get the right to run the HS, but when the winner would go down for some reason, the others would automatically take over after @RendPostPeriod@, which is by default one hour. We could lower that delay to make the downtime lower as well, but this makes hidden services more "noisy" and therefore easier to discover. Since this is not a problem for us (the hidden service is public), something like 5 minutes could be a better setting.
