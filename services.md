This is tightly coupled with the maintenance of the [atheme-services debian package](https://tracker.debian.org/pkg/atheme-services). Usually, the first step in setting up a new host or updating it is to update the Debian package, which is not documented here for now. So let's assume there's a version up to date in unstable for now.

We are assuming here you are running Debian stable on a fully configured IRC server (with Charybdis).

Documentation for atheme services can be found on the [official wiki](https://github.com/atheme/atheme/wiki).

[[_TOC_]]

Installing
======

The version of atheme in stable is probably outdated and we'll want to use a backport. Hopefully, the version in Debian will already up to date, so you will simply run:

```
sudo apt install -t jessie-backports atheme-services
```

If it's not available publicly or not up to date, you can build a backport by checking out the latest source from stretch and building it in a Jessie host:

```
git clone -b jessie-backports git+ssh://git.debian.org/git/collab-maint/atheme-services.git
sudo DIST=jessie ARCH=amd64 git-buildpackage --git-debian-branch=jessie-backports
```

If there is no appropriate branch for the current distribution, you can still build the package:

```
apt-get source -t testing atheme-services
cd atheme-services*
dch --bpo
debuild
```

Ideally, you would build those on your own computer in a clean Jessie chroot, and then copy the files over to the server to avoid having all the build tools on the server.

Configuration
=========

Charybdis
--------------

Charybdis should *already* be configured, if you are using the shared config (which you should), with a block like:

```
connect "services.indymedia.org" {
        host = "127.0.0.1";
        send_password = "[censored]";
        accept_password = "[censored]";
        port = 6667;
        hub_mask = "*";
        class = "server";
        flags = topicburst;
};
```

Atheme
------------

Upon install, the Debian packages does absolutely no configuration. You need to copy files from `/usr/share/doc/atheme-services/examples` to make a new setup. We have not done that for over a decade, however, so you will probably be upgrading from a previous config, see the Upgrading section below.

At the very least, there should be a matching `connect` block for the above IRC server.

`ENABLE=1` needs to be set in `/etc/default/atheme-services` for the service to start properly.

Upgrading
=======

Merging the `atheme.conf` file consists mostly at discovering new modules, new functionalities which can be shared with the users.

First, a git tag is laid against the current config, then the new config is put in place in a separate branch. Then the two branches are merged, and changes are cherry-picked between the two:

```
cd /etc/atheme
git tag 6.0.11
git checkout -b 7.0.7
cp /usr/share/doc/atheme-services/examples/atheme.conf.example atheme.conf
git commit -m"new upstream config" atheme.conf
git checkout master
git merge 7.0.7
# you probably want to redo the merge by hand
git checkout 6.0.11 atheme.conf
git reset atheme.conf
git add -p atheme.conf # re-add the parts of the old config we need
git checkout -p atheme.conf # restore the upstream configs we want
git commit -m"upgrade to 7.0.7"
```

This is obviously be a little too complicated: the simplest way to manage this would be for the Debian package to ship a `/etc/atheme/atheme.conf` default config.

6 to 7.0.7 upgrade
--------------------------

Changes noted in the config:

* new functionalities:
  * warn before clones limit is hit (4/5)
  * identified users do not count towards the clones limit, up to double the limit
* new modules:
  * Bad email address blocking (`nickserv/badmail`)
  * LISTGROUPS command (`nickserv/listgroups`)
  * RESTRICT command (`nickserv/restrict`)
  * CLONE command (`chanserv/clone`)
  * SET NEVERGROUP command (`nickserv/set_nevergroup`)
  * SET NEVEROP command (`nickserv/set_neverop`)
  * SET NOGREET command (`nickserv/set_nogreet`)
  * Moderated channel registration (`chanserv/moderate`, disabled)
  * SYNC command (and automatic ACL syncing, `chanserv/sync`)
  * SET command for OperServ to modify temporarily config options (`operserv/set`)
  * StatServ (adds basic stats and split tracking, main, netsplit and server enabled, channel disabled)
  * RPGserv (provides a facility for finding roleplaying channels, disabled)
  * ChanFix (provides recovery services without registration, disabled)
* wants to be disable, we keep:
  * `nickserv/access`
  * `nickserv/cert`
  * `nickserv/enforce`
  * `nickserv/fflags`
  * `nickserv/xop`
  * `saslserv/dh-blowfish`
  * `operserv/clearchan`
  * `operserv/clones`
  * `operserv/rakill`
  * `operserv/soper`
* wants to enable, we keep disabled:
  * nickserv spam setting
  * nickserv and chanserv 30 day expiry (we keep 180 days)
  * httpd server wants to come back

Note that there's a now [Web UI called SWI](https://atheme.github.io/swi.html) as well, which could allow
users to register through Tor to join the network anonymously.

Backups
======

A full backup of the configuration and databases of atheme can be performed with:

```
tar cfz /var/backups/atheme-services-$(date %+Ymd).tgz /etc/atheme /var/lib/atheme
```

This omits logfiles.

Migrating to another host
==================

Migrating to another host is basically all of the above:

* install charybdis
* install atheme (see "Installing" above)
* copy the config (`atheme.conf`) over and upgrade it (see "Upgrading" above)
* fix `connect` block in charybdis
* fix `uplink` block in atheme
* stop services on current host: `service atheme-services stop` (ignore if old host is down, obviously)
* copy `/var/lib/atheme/services.db` from old host to new host (or restore from backups)
* start atheme on new host

The secret channel
===============

Services are confiured to hang out in a channel and post some information there that's not for most user's eyes (might contain information on some users like IP addresses or email addresses linked to accounts).

That channel needs to be restricted so that only authenticated network operators can join. There's a magic mode that we can set manually to setup the desired restriction:

```
/mode #services +O
```

Unfortunately, this flag currently (2020-05) won't stick around when we restart atheme and/or charybdis on chat1 even though the channel has mode `+P` which charybdis developers said it should make channel modes persist even if it gets emptied of all users.

The `+O` flag seems to be a shortcut for `+irc` and `+I $o`. We can add the `+irc` options to the MLOCK to make them permanent. TODO: verify that `+irc` modes persist through restarts of charybdis on chat1 in the future.

However the invite exemption list might still get cleared if the channel is destroyed (presumably that's what's happening since flags get cleared out). To check whether the invite exemption is still present, one can list them with:

```
/mode #services +I
```

there should be an entry that looks like the following:

```
16:38 -!- #services: invite $o [by LeLutin!~lelutin@koumbit.org, 13521 secs ago]
```

If the list is empty, you can place the invite exemption back with:

```
/mode #service +I $o
```
