We have a shared configuration among all the IRC servers in `/etc/charybdis/sharedconf`. That configuration is committed into git and should be the same on *all* servers. You can *clonse* that directory directly and push to it, provided you are part of the `ircd` group (which you should be if you are an operator).

`/etc` is also under version control with `etckeeper`. Please do commit changes with a meaningful commit log. For more far-ranging changes, edit `/etc/changelog`.

[[_TOC_]]

Applying configuration changes
=====================

When modified, the configuration should be reloaded with `service charybdis reload`. Important: on some servers, the reload may *not* work, as the `init.d` script has a bug where the signal is not delivered correctly. If the motd doesn't mention the `init.d` is patched, you probably need to find the process yourself (`ps ax | grep charybdis`) and delivery the signal yourself (`kill -HUP $pid`). The bug is present in charybdis 3.5.0-1 and previous.

Propogating configuration changes
=================

All the important configuration for the IRC servers lives in `/etc/charybdis/sharedconf`, which is a git repository *separate* from the parent `/etc` repository. Changes can be pushed and pulled from thar repository, because it is setup with `core.sharedRepository` set to `group`. This picture shows basically how this works:

![diagram](sharedconf/after.png)

To clone the configuration and setup all branches:

```
git clone chat1.koumbit.net:/etc/charybdis/sharedconf
cd sharedconf
git remote rename origin chat1
git remote add chat0 chat0.koumbit.net:/etc/charybdis/sharedconf
git fetch chat0
git remote add che che.indymedia.org:/etc/charybdis/sharedconf
git fetch che
```

On 2019-11-13, the following configuration was performed on `che`:

```
addgroup ircop
for op in $ALL_OPS ; do adduser $op ircop ; done
cd /etc/charybdis/sharedconf
git config receive.denyCurrentBranch updateInstead
git config core.sharedRepository group
chown -R :ircop .
chmod g+rwX -R .
chmod a+X .
```

The downside of this approach is it removes the "two-factor authentication" (SSH keys and password) we currently have (more or less) to change configurations.

The clunky (old) way
-----------------------------

This is the way anarcat built the push/pull system originally, but it was very clunky. The idea was to have a user-writable repository in my home directory, and push/pull changes to that with all hosts. Maybe an image would work a little better:

![](sharedconf/before.png)

Obviously, this would be simpler if `/etc/chardybdis/sharedconf` was a "shared" git repository (all users in the same group with write access, see `core.sharedRepository` in `git-config(1)`. A post-receive hook to reset the checkout would also need to be added, like [post-receive-checkout-force](https://redmine.koumbit.net/projects/git-hooks/repository/revisions/master/entry/post-receive-checkout-force) or [post-receive-checkout-copy](https://redmine.koumbit.net/projects/git-hooks/repository/revisions/master/entry/post-receive-checkout-copy), if we want to keep a separate bare repository.

Also: notice how there is no clear way of sending changes from `/etc/charybdis/sharedconf`! I usually cheat by pushing to my personal repo as root and then fix the permissions with `chown -R anarcat ~/sharedconf.git`.

And yes, this means a fairly elaborate configuration on your laptop. Here is my current list of remotes:

```
anarcat@angela:sharedconf$ git remote -v
chat0   chat0.koumbit.net:sharedconf.git (fetch)
chat0   chat0.koumbit.net:sharedconf.git (push)
chat1   chat1.koumbit.net:sharedconf.git (fetch)
chat1   chat1.koumbit.net:sharedconf.git (push)
che     che.indymedia.org:sharedconf.git (fetch)
che     che.indymedia.org:sharedconf.git (push)
guerin  guerin.indymedia.org:sharedconf.git (fetch)
guerin  guerin.indymedia.org:sharedconf.git (push)
```

Also note that the `/etc/charybdis/ircd.conf` file is specific to each server. Still, it is managed in the `sharedconf` directory through a symlink to `/etc/charybdis/sharedconf/ircd-$(hostname -s).conf`. This allows easier comparison between the server of this crucial configuration file.

h3. Downsides

Obviously, this setup is too complicated. It could be better to have a central git host - but then we have a central point of failure to exchange files. It also means setting up SSH connexions among the servers, which implies that a compromise on one server could extend on the other servers. Maybe this could be limited with the use of a restricted `git-shell` and a special user on some host, but then you quickly get into key management problems.

The setup above is especially complicated because users don't have write access to `/etc/charybdis/sharedconf` without first going through `sudo`.


Configuration management
--------------------------------------

Another alternative would be to use more elaborate configuration management like Puppet, Ansible or Propellor (why not!) to manage the nodes. The downsides there are similar: a Puppetmaster is completely centralized and a new attack vector, Ansible requires password-less sudo access, and Propellor needs a shared git repository as well.

The task of cleaning up that stuff is assigned and tracked as part of the main [todo] list.

Update: note that Puppet is currently in use in some of the nodes. In particular, che has some Puppet configuration that is manually ran out of `/root/puppet`.

Configuration management problems
==========================

There are a few configuration management problems to solve on the cluster right now.

1. user, password and SSH key setup - currently done by hand
2. charybdis code deployment - currently done through the debian package and "by hand" deployment (`scp` to each host and `dpkg -i`)
3. charybdis configuration - currently done with git with the above convoluted setup
4. tor hidden service configuration - currently done with a master-less puppet setup
5. TLS/X509 certificates deployment - currently done automatically with let's encrypt on `chat1`, certificates are synchronized by hand

We should find a unified solution for all of this. Here are the solutions proposed so far:

* keep using only git, with a centralized repository (say on [0xacab](https://0xacab.org/)) and possibly with git-gcrypt encryption. downside: doesn't address parts 1, 4 and 5 above
* use a master-less Puppet setup. already setup on `che.indymedia.org:/root/puppet`. downside: syncing the configurations still requires a central repo. workflow unclear to do deployments (how do we pull the code on each server? by hand?)
* create a new Ansible playbook. downside: syncing configurations still requires a central repo. some people less familiar with ansible.
* use [DNS authentication](https://certbot-dns-rfc2136.readthedocs.io/en/latest/) for X509 certificates renewals. allows per-host real names (e.g. you can connect to "che.indymedia.org" and the hostname will match). downside: requires access (delegation!) to indymedia.org (and koumbit.net!) records

Graphs source code
==================

The above graphs were generated with the following dot files:

```
digraph sharedconf {
        guerinsharedconf [ label="guerin:/etc/charybdis/sharedconf" ]
        guerinsharedconfbare [ label="guerin:/home/anarcat/sharedconf.git (bare)" ]
        chesharedconf [ label="che:/etc/charybdis/sharedconf" ]
        chesharedconfbare [ label="che:/home/anarcat/sharedconf.git (bare)" ]
        { rank = sink;
                laptop [ label="laptop:/home/anarcat/sharedconf";]
        }
        guerinsharedconfbare -> guerinsharedconf [ label="local pull as root" ]
        laptop -> guerinsharedconfbare  [ label="pull/push over ssh" ]
        chesharedconfbare -> chesharedconf [ label="local pull as root" ]
        laptop -> chesharedconfbare [ label="pull/push over ssh" ]
}
```

```
digraph sharedconf {
        guerinsharedconf [ label="guerin:/etc/charybdis/sharedconf" ]
        chesharedconf [ label="che:/etc/charybdis/sharedconf" ]
        { rank = sink;
                laptop [ label="laptop:/home/anarcat/sharedconf";]
        }
        laptop -> guerinsharedconf  [ label="pull/push over ssh" ]
        laptop -> chesharedconf [ label="pull/push over ssh" ]
        etc [ label = "..." ]
        laptop -> etc
}
```
